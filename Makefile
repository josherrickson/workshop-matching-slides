
.PHONY:default
default:
	quarto render

.PHONY:open
open:
	@open docs/index.html

.PHONY: check-valid-public-site
check-valid-public-site:
	@if [ ! -d "../public-site" ]; then \
		echo "../public-site does not exist."; \
		exit 1; \
	fi
	@if [ ! -d "../public-site/.git" ]; then \
		echo "../public-site is not a Git repository."; \
		exit 1; \
	fi

.PHONY: publicize
publicize: check-valid-public-site
	@echo Syncing to ../public-site/public
	@mkdir -p ../public-site/public/workshop-matching-slides
	@rsync -rv --delete docs/ ../public-site/public/workshop-matching-slides
